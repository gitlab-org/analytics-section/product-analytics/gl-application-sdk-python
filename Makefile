.PHONY: build

SHELL:=/bin/zsh

.DEFAULT: help

help:
	@echo "\n \
	------------------------------------------------------------ \n \
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n \
	python-linter: Run linters for the code \n \
	------------------------------------------------------------"
isort:
	isort ./**/*.py
black:
	black ./**/*.py
mypy:
	mypy . --ignore-missing-imports --exclude=build
pylint:
	pylint ./**/*.py --disable=line-too-long,E0401,E0611,W1203,W1202,C0103,R0902,W0212 --ignore-paths=build
flake8:
	flake8 ./**/*.py --ignore=E203,E501,W503,W605 --exclude=build/**/*
vulture:
	vulture ./**/*.py --min-confidence 70

python-linter: isort black flake8 vulture pylint mypy
	@echo "Running python_code_quality..."
