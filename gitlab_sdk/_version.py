"""
Module to manage version information for the gitlab_sdk package.
"""
__version_info__ = (0, 1, 0)
__version__ = ".".join(str(x) for x in __version_info__)
__build_version__ = __version__ + ""
