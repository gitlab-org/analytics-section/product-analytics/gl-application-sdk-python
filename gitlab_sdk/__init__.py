"""
Main module for gitlab_sdk. Exposes version and the main Client class.
"""
from gitlab_sdk._version import __version__
from gitlab_sdk.client import Client
