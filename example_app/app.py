from flask import Flask
from gitlab_sdk import Client
import os

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Flask server.</p>"

@app.route("/api/v1/send_event")
def send_event():
    client = Client(app_id=os.environ['PA_APPLICATION_ID'], host=os.environ['PA_COLLECTOR_URL'])
    client.identify(user_id='123abc', user_attributes={ "user_name": "UserName" })
    client.track(event_name='An event', event_payload={ "id": "321" })
    return "<p>Event sent!</p>"
